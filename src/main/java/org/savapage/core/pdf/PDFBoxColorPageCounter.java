/*
 * This file is part of the SavaPage project <https://www.savapage.org>.
 * Copyright (c) 2011-2025 Datraverse B.V.
 * Author: Rijk Ravestein.
 *
 * SPDX-FileCopyrightText: 2011-2025 Datraverse B.V. <info@datraverse.com>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * For more information, please contact Datraverse B.V. at this
 * address: info@datraverse.com
 */
package org.savapage.core.pdf;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.pdfbox.Loader;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.savapage.core.inbox.RangeAtom;

/**
 * Counts number of color pages in PDF.
 *
 * @author Rijk Ravestein
 *
 */
public final class PDFBoxColorPageCounter {

    /**
     * Image resolution.
     *
     * @author Rijk Ravestein
     *
     */
    public enum ImageRenderResolution {
        /** Good enough to detect color. */
        DPI_36(36f),
        /** Suitable for web pages. */
        DPI_72(72f);

        /** */
        private final float dpi;

        ImageRenderResolution(final float res) {
            this.dpi = res;
        }

        /**
         * @return resolution in DPI.
         */
        public float getDPI() {
            return this.dpi;
        }
    }

    /** Utility class. */
    private PDFBoxColorPageCounter() {
    }

    /**
     * Checks if RGB image is monochrome.
     *
     * @param imageRGB
     * @return {@code true} if monochrome.
     */
    private static boolean isMonochrome(final BufferedImage imageRGB) {

        final int width = imageRGB.getWidth();
        final int height = imageRGB.getHeight();

        for (int x = 0; x < width; x++) {

            for (int y = 0; y < height; y++) {

                final Color pixelColor = new Color(imageRGB.getRGB(x, y));
                final int green = pixelColor.getGreen();

                /*
                 * Monochrome: Red = Green = Blue.
                 */
                if (pixelColor.getRed() != green
                        || green != pixelColor.getBlue()) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Calculates number of color page ranges in PDF.
     *
     * @param pdfFile
     * @return color page ranges.
     * @throws IOException
     */
    public static List<RangeAtom> count(final File pdfFile) throws IOException {

        final List<RangeAtom> colorRanges = new ArrayList<>();

        int nColorStart = 0;

        try (PDDocument document = Loader.loadPDF(pdfFile);) {

            final PDFRenderer pdfRenderer = new PDFRenderer(document);

            for (int page = 0; page < document.getNumberOfPages(); ++page) {

                final BufferedImage imageRGB = pdfRenderer.renderImageWithDPI(
                        page, ImageRenderResolution.DPI_36.getDPI(),
                        ImageType.RGB);

                if (isMonochrome(imageRGB)) {
                    if (nColorStart > 0) {
                        colorRanges.add(
                                RangeAtom.fromPageRange(nColorStart, page));
                        nColorStart = 0;
                    }
                } else {
                    if (nColorStart == 0) {
                        nColorStart = page + 1;
                    }
                }
            }
            if (nColorStart > 0) {
                colorRanges.add(RangeAtom.fromPageRange(nColorStart,
                        document.getNumberOfPages()));
            }
        }
        return colorRanges;
    }

}
